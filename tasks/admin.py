from django.contrib import admin
from tasks.models import Task


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    task_display = [
        "name",
        "start date",
        "due date",
        "is copmlete",
        "project",
        "assignee",
    ]
